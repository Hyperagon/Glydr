﻿using UnityEngine;
using System.Collections;

public class LoopingBackground : MonoBehaviour {

    GameObject background;

	// Use this for initialization
	void Start () {
        background = gameObject;
	}

    static float move = -45;

	// Update is called once per frame
	void FixedUpdate () {

        if (GameManager.StartGame) {
            move += 0.05f;
            gameObject.transform.position = new Vector3(0, move, 0);
            if (IsAboveVisibleScreen(transform)) {
                move = -45;
            }
        }

    }

    private bool IsAboveVisibleScreen (Transform t) {
        //really wanting to get rid of the new () allocation here. Should use static, will check with guide.

        SpriteRenderer spriteR = t.GetComponent<SpriteRenderer>();
        //Pick a point on the lower edge
        Vector3 lowestPosition = new Vector3(spriteR.bounds.center.x, spriteR.bounds.center.y - spriteR.bounds.size.y / 2.0f, spriteR.bounds.center.z);

        Vector3 screenCoords = Camera.main.WorldToScreenPoint(lowestPosition);
        if (screenCoords.y > Screen.height) {
            return true;
        }
        //Debug.Log( "Screen coord Y = " + screenCoords.y);

        return false;
    }
}
