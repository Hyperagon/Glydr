﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int Score = 0;

    public Text scoreText = null;
    public Text GameOverScoreText = null;
    public Text highscoreText = null;

    private GameManager gManager;

    //private int currentHighScore;
    private int easyHighScore = 0;
    private int normalHighScore = 0;
    private int hardHighScore = 0;
    public int shownHighScore = 0;

    void Start()
    {
        gManager = GetComponent<GameManager>();
        HighScoreFromMemory();
    }

    void Update()
    {
        UpdateScoreText(Score);
        switch (gManager.gamemode)
        {
            case GameManager.Difficulty.Easy:
                if (Score > easyHighScore)
                {
                    UpdateHighScore(Score, GameManager.Difficulty.Easy);
                }
                break;
            case GameManager.Difficulty.Normal:
                if (Score > normalHighScore)
                {
                    UpdateHighScore(Score, GameManager.Difficulty.Normal);
                }
                break;
            case GameManager.Difficulty.Hard:
                if (Score > hardHighScore)
                {
                    UpdateHighScore(Score, GameManager.Difficulty.Hard);
                }
                break;
            default:
                break;
        }
    }

    private void HighScoreFromMemory()
    {
        if (PlayerPrefs.HasKey("easy_highscore"))
        {
            easyHighScore = PlayerPrefs.GetInt("easy_highscore");
            Debug.Log(easyHighScore);
        }
        else
        {
            PlayerPrefs.SetInt("easy_highscore", 0);
        }

        if (PlayerPrefs.HasKey("normal_highscore"))
        {
            normalHighScore = PlayerPrefs.GetInt("normal_highscore");
            Debug.Log(normalHighScore);
        }
        else
        {
            PlayerPrefs.SetInt("normal_highscore", 0);
        }

        if (PlayerPrefs.HasKey("hard_highscore"))
        {
            hardHighScore = PlayerPrefs.GetInt("hard_highscore");
            Debug.Log(hardHighScore);
        }
        else
        {
            PlayerPrefs.SetInt("hard_highscore", 0);
        }
    }

    public void UpdateScoreText(int s)
    {
        scoreText.text = s.ToString();
    }

    public void UpdateHighScore(int hs, GameManager.Difficulty d)
    {
        switch (d)
        {
            case GameManager.Difficulty.Easy:
                PlayerPrefs.SetInt("easy_highscore", hs);
                easyHighScore = Score;
                break;
            case GameManager.Difficulty.Normal:
                PlayerPrefs.SetInt("normal_highscore", hs);
                normalHighScore = Score;
                break;
            case GameManager.Difficulty.Hard:
                PlayerPrefs.SetInt("hard_highscore", hs);
                hardHighScore = Score;
                break;
            default:
                break;
        }
    }

    public void GameOver(GameManager.Difficulty difficulty)
    {
        switch (difficulty)
        {
            case GameManager.Difficulty.Easy:
                shownHighScore = easyHighScore;
                break;
            case GameManager.Difficulty.Normal:
                shownHighScore = normalHighScore;
                break;
            case GameManager.Difficulty.Hard:
                shownHighScore = hardHighScore;
                break;
            default:
                break;
        }
        highscoreText.text =  " HighScore \n" + difficulty + " :" + shownHighScore;
        GameOverScoreText.text = Score.ToString();
    }



}


