﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    private int wallSpeed = 7;
    private int playerSpeed = 40;
    private ScoreManager sManager;
    private GameObject boy;
    private Animator anim;

    public enum Difficulty { Easy, Normal, Hard };
    public Difficulty gamemode = Difficulty.Normal;
    public GameObject gameOverPanel;
    public BuildingScrolls WallMgr;
    public PlayerScript Player;
    public bool ifStartAnimation;
    public static bool StartGame = false;

    void Start()
    {
        if(Player.showOnStart)
        {
            Player.gameObject.SetActive(true);
        }
        else
        {
            Player.gameObject.SetActive(false);
        }
    }

    private void Update() {
        SetBrokenWindows();
    }

    private void SetBrokenWindows () {
        if (gamemode == Difficulty.Hard) {
            switch (ScoreManager.Score) {
                case 0:
                    WallPiece.minBroken = 2;
                    WallPiece.maxBroken = 2;
                    break;

                case 25:
                    WallPiece.minBroken = 3;
                    break;

                case 50:
                    WallPiece.maxBroken = 3;
                    break;

                default:
                    break;
            }
        } else {
            switch (ScoreManager.Score) {
                case 0:
                    WallPiece.minBroken = 1;
                    WallPiece.maxBroken = 1;
                    break;

                case 10:
                    WallPiece.maxBroken = 2;
                    break;

                case 25:
                    WallPiece.minBroken = 2;
                    break;

                case 50:
                    WallPiece.maxBroken = 3;
                    break;

                case 100:
                    WallPiece.minBroken = 3;
                    break;

                default:
                    break;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player") {
            switch (gamemode) {
                case Difficulty.Easy:
                Player.onWall = true;
                StartCoroutine(Player.offWall());
                break;

                case Difficulty.Normal:
                case Difficulty.Hard:
                    GameOver();
                    Player.DeathSequence();
                    WallMgr.speed = 0f;
                    break;
            }
        }
    }

    private IEnumerator Throw()
    {
        if (ifStartAnimation)
        {
            anim = boy.GetComponent<Animator>();
            anim.SetTrigger("Throw");
            yield return new WaitForSeconds(0.2f);
        }
        else
        {
            yield return new WaitForSeconds(0.1f);
            Player.transform.position = Player.spawn.transform.position;
            Player.gameObject.SetActive(true);
            Player.acceptInput = true;
            Player.maxSpeed = playerSpeed;
            WallMgr.speed = wallSpeed;
            if(!Player.isRotatable)
            {
                Player.Flip();
            }
            else
            {
                Player.Rotate();
            }
        }
        
        
    }

    public void GameOver()
    {
        StartGame = false;
        sManager = GetComponent<ScoreManager>();
        sManager.GameOver(gamemode);
        gameOverPanel.SetActive(true);
    }

    public void ResetTheGame() {
        ScoreManager.Score = 0;
        SetBrokenWindows();
        WallMgr.ResetTheWalls();
        Player.ResetPlayer(); //reset position
        gameOverPanel.SetActive(false);
    }

    public void SetDifficulty(int d)
    {
        switch(d)
        {
            case 0:
                playerSpeed = 40;
                wallSpeed = 7;
                gamemode = Difficulty.Easy;
                break;
            case 1:
                playerSpeed = 40;
                wallSpeed = 7;
                gamemode = Difficulty.Normal;
            break;

            case 2:
            playerSpeed = 50;
            wallSpeed = 8;
            gamemode = Difficulty.Hard;
            break;

        }

    }

    public void StartTheGame() {
        StartGame = true;

        if (ifStartAnimation)
        {
            boy = GameObject.FindGameObjectWithTag("Boy");
        }
        StartCoroutine(Throw());
    }

    public void PausedGame(bool paused)
    {
        if(paused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

}

