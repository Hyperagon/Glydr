﻿using UnityEngine;

public class WinScript : MonoBehaviour {
    public GameObject normalWindow;
    public GameObject brokenWindow;
    //unused public GameObject burstingWindow;

    // Use this for initialization
    private void Start() {
    }

    // Update is called once per frame
    private void Update() {
    }

    public void ResetScript() {
        brokenWindow.SetActive(false);
        normalWindow.SetActive(true);
    }

    //returns (true) if the window is broken or about to burst, in English format.
    public bool IsBroken() {
        return !normalWindow.activeSelf;
    }

    public void DoBreak() {
        brokenWindow.SetActive(true);
        normalWindow.SetActive(false);
    }
}