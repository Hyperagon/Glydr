﻿using UnityEngine;

public class BurstScript : MonoBehaviour {
    public int rndTopNum = 5;
    private BoxCollider2D theBox;
    private Animator anim;
    //public GameObject _fire;

    private void Awake() {
        anim = GetComponent<Animator>();
        theBox = GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            int rndNumber = Random.Range(0, rndTopNum);

            if (rndNumber > 2) {
                Debug.Log("Kill on");
                anim.SetTrigger("Burst");
                //Instantiate(_fire, transform.position, transform.rotation);
                theBox.enabled = true;
            } else {
                Debug.Log("Kill off");
            }
        }
    }
}