﻿using UnityEngine;

public class CloudScrollingTitle : MonoBehaviour {
    public Transform[] clouds;
    public float speed = 2.5f;

    // Use this for initialization
    private void Start() {
    }

    // Update is called once per frame
    private void Update() {
        float cloudMovement = Time.deltaTime * speed;

        foreach (Transform cloud in clouds) {
            cloud.Translate(Vector3.up * cloudMovement);

            if (cloud.position.y > 11.0f) {
                cloud.position = new Vector3(cloud.position.x, 0, cloud.position.y);
            }
        }
    }
}