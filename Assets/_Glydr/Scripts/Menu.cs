﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Menu : MonoBehaviour {

    //public int scene = 0;
    private bool continueLoop = true;
    private Slider progressBar;
    public Slider City;
    public Slider Space;

    private AsyncOperation async;

    public void StartSpace(int scene)
    {
        switch (scene)
        {
            case 1:
                progressBar = Space;
                break;
            case 2:
                progressBar = City;
                break;
            default:
                break;
        }
        StartCoroutine(loading(scene));
    }

    public IEnumerator loading(int sceneToLoad)
    {

        async = SceneManager.LoadSceneAsync(sceneToLoad);
        if (sceneToLoad > 0)
        {
            while (async.progress <= .89)
            {
                progressBar.value = async.progress;
                //yield return new WaitForFixedUpdate();
            }
        }
        yield return null;
    }
}
