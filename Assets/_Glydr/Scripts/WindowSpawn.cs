using UnityEngine;

public class WindowSpawn : MonoBehaviour {
    public GameObject[] panel;
    public int poolSize = 6;
    private int currentPanel = 0;
    private GameObject[] panelGroup;
    public float panelPosition = -20f;

    private void Start() {
        panelGroup = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++) {
            int getPanel = Random.Range(0, panel.Length);
            panelGroup[i] = (GameObject)Instantiate(panel[getPanel]);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Panel") {
            Vector3 pos = new Vector3(0, panelPosition);
            panelGroup[currentPanel].transform.position = pos;
            pos.y = panelPosition -= 10;
            if (++currentPanel >= poolSize) {
                currentPanel = 0;
            }
        }
    }
}