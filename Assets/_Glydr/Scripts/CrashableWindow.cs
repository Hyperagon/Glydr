﻿using UnityEngine;

public class CrashableWindow : MonoBehaviour {
    public PlayerScript Player;

    // Use this for initialization
    //	public void Start ()
    //	{
    //		 //easier than assigning every single broken win.
    //	}

    private GameObject playerObject;

    private void OnCollisionEnter2D(Collision2D collision) {
        playerObject = GameObject.FindGameObjectWithTag("Player");

        if (playerObject) {
            Player = playerObject.GetComponent<PlayerScript>();
        }
        if (collision.gameObject.tag == "Player") {
            Player.DeathSequence();
            //Let the player handle calling the game manager
        }
    }
}