﻿using UnityEngine;
using System.Collections;


public class PlayerScript : MonoBehaviour {

    

    public float maxSpeed = 10f;
    public float move = 1f;
    public float smoothTime = 0.3f;
    public float particleDirection;

    public bool showOnStart = true;
    public bool isRotatable = false;
    public bool onWall = false;
    public bool acceptInput = false;
    public bool isDead = false;

    public GameObject spawn;
    public GameManager gManager;
    public ParticleSystem particles;
    public int rotationAngle = 30;

    private float xVelocity = .01f;
    private bool facingRight = true;
    private Rigidbody2D rigid;

    public float viewRotation;


    private void Start() {
        rigid = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        viewRotation = rigid.rotation;

        if(acceptInput)
        {
            if(Input.anyKeyDown || TappedPhone()) //for movement, game start is handled by ui
            {
                if(!isRotatable)
                {

                    Flip();
                }
                else
                { 
                    Rotate();
                }
            }
        }
    }
    private bool TappedPhone() {
        return (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began);
    }

    public IEnumerator offWall()
    {
        if(!isRotatable)
        {
            yield return new WaitForSeconds(0.05f);
            Flip();
            yield return new WaitForSeconds(0.05f);
            onWall = false;
        }
        else
        {
            yield return new WaitForSeconds(0.05f);
            Rotate();
            yield return new WaitForSeconds(0.05f);
            onWall = false;
        }
    }

    private void FixedUpdate() {
        if(!isDead)
        {
            float positionSmooth = Mathf.SmoothDamp(rigid.velocity.x * move, maxSpeed, ref xVelocity, smoothTime);
            rigid.velocity = new Vector2(move * positionSmooth, 0);
            if(acceptInput)
            {
                rigid.constraints = RigidbodyConstraints2D.None;
                if(isRotatable)
                {
                    if(move < 0)
                    {
                        if(rigid.rotation > -180 + rotationAngle)
                        {
                            rigid.angularVelocity = -200;
                        }
                        else
                        {
                            rigid.angularVelocity = 0;
                        }
                    }
                    else if(move > 0)
                    {
                        if(rigid.rotation < -180 + rotationAngle)
                        {
                            rigid.angularVelocity = 200;
                        }
                        else
                        {
                            rigid.angularVelocity = 0;
                        }
                    }
                    else
                    {
                        rigid.angularVelocity = 0;
                    }
                }
            }
            else
            {
                rigid.constraints = RigidbodyConstraints2D.FreezePositionX;
            }
        }

    }

    public void Flip() {
        move *= -1;
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        onWall = false;
    }

    public void Rotate()
    {
        move *= -1;
        facingRight = !facingRight;
        //gameObject.transform.eulerAngles = new Vector3(0, 0, 180 - rotationAngle);
        rotationAngle *= -1;
        onWall = false;
    }

    //Bursting windows will test for the player. Player needs to do no tests for collide.
    public BuildingScrolls scrollScript;

    public void DeathSequence() {
        isDead = true;
        scrollScript.speed = 0;

        gameObject.SetActive(false);
        gManager.GameOver();
        acceptInput = false;

    }

    public void ResetPlayer() {
        maxSpeed = 0;
        transform.localScale = new Vector3(1,1,1);
        Rotate();
        move = 1;
        if(showOnStart)
        {
            rotationAngle = 30;
            transform.eulerAngles = new Vector3(0, 0, 180);
            gameObject.SetActive(true);
        }
        isDead = false;
        transform.position = spawn.transform.position;


    }
}