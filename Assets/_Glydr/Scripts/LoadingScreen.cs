using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour {
    //I still need to test this and customize it since this is a copy paste from a video.

    public int loadLvl;

    public GameObject loadingScreen;
    public GameObject progressBar;
    public Text loadingText;
    public GameObject startButton;

    private int loadProgress = 0;
    private AsyncOperation async;

    public void LoadGame() {
        StartCoroutine(DisplayLoadingScreen(loadLvl));
        // async.allowSceneActivation = false;
    }

    private IEnumerator DisplayLoadingScreen(int level) {
        progressBar.transform.localScale = new Vector3(loadProgress, progressBar.transform.localScale.y, progressBar.transform.localScale.z);

        loadingText.text = "Loading Progress " + loadProgress + "%";

        async = SceneManager.LoadSceneAsync(loadLvl);

        // async.allowSceneActivation{async.progress == 1};
        while (async.progress <= .90) {
            loadProgress = (int)(async.progress * 100);
            if (async.progress >= .89) {
                loadingText.text = "Ready to play!";
                startButton.SetActive(true);
            } else {
                loadingText.text = "Loading Progress " + loadProgress + "%";
            }

            //loadingText.text = "Loading Progress " + loadProgress + "%";

            progressBar.transform.localScale = new Vector3(async.progress, progressBar.transform.localScale.y, progressBar.transform.localScale.z);
            yield return null;
        }
    }

    public void StartGame() {
        async.allowSceneActivation = true;
    }
}