﻿using System;
using UnityEngine;

public class WallPiece : MonoBehaviour {
    public GameObject[] Windows;
    public BoxCollider2D PastObstaclesCollider; //Past the windows for this version, generic name for more types of games.
    public static int maxBroken = 1;
    public static int minBroken = 1; //may move to more global class, higher on the tree for better determining breaking.

    public GameManager gManager;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player" && HasBrokenWindow()) {
            PastObstaclesCollider.enabled = false; //disable to prevent double score updating. Re-enable when recycled.
            ScoreManager.Score += 1;
            //Debug.Log("score:" + gManager.TheScore);
        }
    }

    private bool HasBrokenWindow() {
        GameObject tempWindow;
        WinScript ws;

        for (int i = 0; i < Windows.Length; i++) {
            tempWindow = Windows[i] as GameObject;
            ws = tempWindow.GetComponent<WinScript>();
            if (ws.IsBroken()) {
                return true;
            }
        }
        return false;
    }

    public void ResetWithWindowDist(float wDistance) {
        GameObject tempWindow;
        WinScript ws;

        float maxDistanceFromCenter = (wDistance * (Windows.Length - 1) / 2.0f);
        for (int i = 0; i < Windows.Length; i++) {

            float rnd = UnityEngine.Random.Range(5,-5);
                tempWindow = Windows[i] as GameObject;
                ws = tempWindow.GetComponent<WinScript>();
                tempWindow.transform.localPosition = new Vector3((i * wDistance) - maxDistanceFromCenter, rnd / 10, tempWindow.transform.position.z);
                ws.ResetScript();
            }
        PastObstaclesCollider.enabled = true;
    }

    public void DecideBreaks() {
        int numberBroken = 0;

        //Destroy the first windows for sure.
        for (int i = 0; i < minBroken; i++) {
            BreakWindow((UnityEngine.Random.Range(0, 4) + (DateTime.Now.Millisecond + (int)Time.time)) % 4);
            numberBroken++;
        }

        //Enable chance on the other windows.
        for (int i = numberBroken; i < maxBroken; i++) {
            int OutOfChance = 2; //eg, 1 out of 2
            if (UnityEngine.Random.Range(0, (OutOfChance)) == 0) {
                BreakWindow(UnityEngine.Random.Range(0, Windows.Length - 1));
            }
        }
    }

    //Breaks the next window from the given index. Because it might already be broken, moves on to the next
    private void BreakWindow(int indexOfWindow) {
        WinScript window = Windows[indexOfWindow].GetComponent<WinScript>();

        //The below will be infinite if length messes up OR if all windows are broken.
        int count = 0;
        while (window.IsBroken() == true) {
            indexOfWindow = (indexOfWindow + 11) % 4;

            if (indexOfWindow == Windows.Length) {
                indexOfWindow = 0;
            }
            window = Windows[indexOfWindow].GetComponent<WinScript>();
            count++;

            if (count >= Windows.Length) {
                break;
            }
        }

        window.DoBreak();
    }
}