using System.Collections;
using UnityEngine;

public class BuildingScrolls : MonoBehaviour {
    public Sprite topSprite = null; //The image.
    public Sprite middleSprite = null;
    public Transform firstTop = null;
    private Vector3 initialPosition;
    // Use this for initialization

    public float speed = 9.0f;
    public float windowDist = 3.0f;

    private ArrayList backgrounds = null;

    private int[] lastBroken; //array that holds the values of the previous row, for smarter determining.
                              //not yet needed to be used, for bursting soon.

    private bool breakThisLine = true;

    private void Start() {
        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame() {
        //yield return new WaitForSeconds(0.5f);

        if (firstTop != null) {
            initialPosition = new Vector3(firstTop.position.x, firstTop.position.y, firstTop.position.z);
        }

        backgrounds = new ArrayList();

        foreach (Transform t in transform) {
            backgrounds.Add(t);
        }

        ResetTheWalls();

        yield return null;
    }

    public void ResetTheWalls() {
        //I grab the first value outside of the FOR loop so I can get the initial wall position.
        SpriteRenderer spriteR = firstTop.GetComponent<SpriteRenderer>();
        Transform currentObj = firstTop.transform;
        currentObj.position = initialPosition;
        spriteR.sprite = topSprite;
        WallPiece wP = firstTop.GetComponent<WallPiece>(); //grab the wall piece script.
        Vector3 lastPosition = spriteR.bounds.center;

        wP.ResetWithWindowDist(windowDist); //resets the positions of the window.

        for (int i = 1; i < backgrounds.Count; i++) {
                currentObj = backgrounds[i] as Transform;
                wP = currentObj.GetComponent<WallPiece>();
                wP.ResetWithWindowDist(windowDist);
                spriteR = currentObj.GetComponent<SpriteRenderer>();
                currentObj.position = new Vector3(initialPosition.x, lastPosition.y - spriteR.bounds.size.y, initialPosition.z);
                lastPosition = spriteR.bounds.center;
            if (i > 3) {
                if (breakThisLine == true) {
                    wP.DecideBreaks();
                }
                breakThisLine = !breakThisLine;
            }
        }
    }

    // Update is called once per frame
    private void Update() {
        WallPiece tempWp;

        foreach (Transform t in backgrounds) {
            if (IsAboveVisibleScreen(t)) {
                SetBelowLowestObject(t);
                tempWp = t.GetComponent<WallPiece>();
                tempWp.ResetWithWindowDist(windowDist);
                t.GetComponent<SpriteRenderer>().sprite = middleSprite;
                if (breakThisLine == true) {
                    tempWp.DecideBreaks();
                }
                breakThisLine = !breakThisLine;
            }

            t.position = new Vector3(t.position.x, t.position.y + (speed * Time.deltaTime), t.position.z);
        }
    }

    private bool IsAboveVisibleScreen(Transform t) {
        //really wanting to get rid of the new () allocation here. Should use static, will check with guide.

        SpriteRenderer spriteR = t.GetComponent<SpriteRenderer>();
        //Pick a point on the lower edge
        Vector3 lowestPosition = new Vector3(spriteR.bounds.center.x, spriteR.bounds.center.y - spriteR.bounds.size.y / 2.0f, spriteR.bounds.center.z);

        Vector3 screenCoords = Camera.main.WorldToScreenPoint(lowestPosition);
        if (screenCoords.y > Screen.height) {
            return true;
        }
        //Debug.Log( "Screen coord Y = " + screenCoords.y);

        return false;
    }

    private void SetBelowLowestObject(Transform t) {
        Transform lowest = this.LowestGameObject();
        SpriteRenderer spriteR = lowest.GetComponent<SpriteRenderer>();

        if (lowest != null) { //added 0.18 (or any #) since there is a crease using just the bounds.
            t.position = new Vector3(spriteR.bounds.center.x, lowest.position.y - spriteR.bounds.size.y + 0.0f, t.position.z);
        }
    }

    private Transform LowestGameObject() {
        Transform lowestGameObj = backgrounds[0] as Transform; //safe coding would have me test to see if there is a backgrounds[0] first.
        Transform otherObject;
        for (int i = 1; i < backgrounds.Count; i++) {
            otherObject = backgrounds[i] as Transform; //and test to see if otherObject is null. I'm rushing right now.
            if (otherObject.position.y < lowestGameObj.position.y) {
                lowestGameObj = otherObject;
            }
        }
        return lowestGameObj;
    }
}