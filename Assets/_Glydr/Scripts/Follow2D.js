
#pragma strict

public var target : Transform;
public var player : GameObject;
var smoothTime = 0.3;
private var thisTransform : Transform;
private var velocity : Vector2;
var yOffset = 5;


function Start()
{
	thisTransform = transform;
	player = GameObject.FindGameObjectWithTag("Player");
}

function FixedUpdate() 
{

	if(player)
	{
		thisTransform.position.y = Mathf.SmoothDamp( thisTransform.position.y, target.position.y, velocity.y, smoothTime) - yOffset;
	}
}